const _ = require('lodash');
const model = require('./userModel')
const jwt      = require('jsonwebtoken');
const passport = require('passport');
const consts = require('../../configs/consts')


exports.login = async (req, res, next) => {
  passport.authenticate('local', {session: false}, (err, user, info) => {
    
    if (err || !user) {
        return res.status(400).json({
            message: info ? info.message : 'Login failed',
            user   : user
        });
    }

    req.login(user, {session: false}, (err) => {
        if (err) {
            res.send(err);
        }

        const token = jwt.sign(user.id, consts.passport.secret);

        return res.json({user, token});
    });
  })(req, res);
}
exports.getProfile = async (req, res) => {
  let user_id = req.user.id;
  let user = await model.findById(user_id)
  if(user){
    let user_data = _.omit(user.get({plain:true}),'password') 
    res.json({status:'ok', data:user_data})
  }else{
    res.json({status:'ok', data:{}})
  }
}
exports.signup = (req, res) => {
  try {
    req.checkBody("email","invalid email").notEmpty()
    req.checkBody("password","invalid password").notEmpty()
    req.checkBody("firstname","invalid firstname").notEmpty()
    req.checkBody("lastname","invalid lastname").notEmpty()
    let errors = req.validationErrors();
    if (errors) throw errors;
    let user_data = {
      email:req.body.email,
      password:req.body.password,
      firstname:req.body.firstname,
      lastname:req.body.lastname
    };
  
    model.create(user_data)
    .then(user => {
      let data =   _.pick(user.get({plain: true}),['id'])
      res.json({status:'ok',data:data})
    })
    .catch(err => {
      let errors =  _.map(err.errors, err => _.pick(err,['message', 'path']))
      res.json({status:'fail', errors:errors})
    })

  } catch (error) {
    res.json({status:'fail', errors:error})
  }
  
}

