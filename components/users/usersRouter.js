var express = require('express');
var router = express.Router();
var users = require('./usersController')
const passport = require('passport')

router.post('/signup',users.signup);
router.post('/login',users.login);
router.get('/myprofile',passport.authenticate('jwt', {session: false}),users.getProfile);

module.exports = router;
