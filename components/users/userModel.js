const Sequelize = require('sequelize');
const sequelize = require('../../configs/db');
const bcrypt = require('bcrypt')
const saltRounds = 10
const User = sequelize.define('user', {
    firstname: {
        type: Sequelize.STRING,
        allowNull:false,
        validate: {
            notEmpty: true,
            isAlphanumeric:true
        } 
    },

    lastname: {
        type: Sequelize.STRING,
        
        allowNull:false,
        validate: {
            notEmpty: true,
            isAlphanumeric:true
        } 
    },
   
    email: {
        type: Sequelize.STRING,
        allowNull:false,
        validate: {
            isEmail:true
        }        
    },
    password: {
        type: Sequelize.STRING,
        allowNull:false,
        validate: {
            notEmpty: true
        }
    }
}, {
    indexes: [{
        unique: true,
        fields: ['email']
    }],
    hooks: {
        beforeCreate: (user, options) => {     
            return bcrypt.hash(user.password, saltRounds).then(function(hash) {
                user.password = hash
            });  
        }
    }
});
User.prototype.comparePassword = function (candidatePassword) {
    let password = this.password;
    return new Promise((resolve, reject) => {
        bcrypt.compare(candidatePassword, password).then(res=>{
            return res?resolve(true):reject(false);
        })
    });
};
module.exports = User