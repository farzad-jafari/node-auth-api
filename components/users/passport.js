const passport    = require('passport');
const passportJWT = require("passport-jwt");
const _ = require('lodash')

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;

const UserModel = require('./userModel');
const consts = require('../../configs/consts')

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    async function (email, password, cb) {
        try {
            let user = await UserModel.findOne({where:{email:email}})
            console.log('user', user.get({plain:true}))
            if (user === null) throw "User not found";
            let success = await user.comparePassword(password);
            if (success === false) throw "pass wrong";
            let user_data = _.pick(user.get({plain:true}),['id','email','firstname','lastname'])
            cb(null, user_data, {message: 'Logged In Successfully'})
            
        } catch (error) {
            return cb(null, false, {message: 'Incorrect email or password'});
        }
    }
));
passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : consts.passport.secret
    },
    function (jwtPayload, next) {
        return UserModel.findById(jwtPayload)
            .then(user => {
                return next(null, _.pick(user.get({plain:true}),['id','email']));
            })
            .catch(err => {
                return next(err);
            });
    }
));