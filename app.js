const createError = require('http-errors'),
express = require('express'),
path = require('path'),
cookieParser = require('cookie-parser'),
logger = require('morgan'),
_ = require("lodash"),
helmet = require('helmet'),
bodyParser = require('body-parser'),
expressValidator = require("express-validator");
require('./components/users//passport');

let indexRouter = require('./components/basics/basicsRouter')
let usersRouter = require('./components/users/usersRouter')

var app = express()

/**
 * security middlewares
 * helmet
 */
app.use(helmet())


app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(expressValidator());


app.use('/', indexRouter)
app.use('/users', usersRouter)

// catch 404 and forward to error handler
app.use((req, res, next) =>
  next(createError(404))
)

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  res.json({status:err.status || 500,message:err.message})
})

module.exports = app
