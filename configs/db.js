const Sequelize = require('sequelize');
const dbConf = {
    host: 'localhost',
    dialect: 'postgres',
    port: 5432,
    database: 'farzadDb',
    user: 'postgres',
    password: 'postgrespass',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    operatorsAliases: false
  };

const sequelize = new Sequelize(dbConf.database, dbConf.user, dbConf.password, {
    host: dbConf.host,
    port:dbConf.port,
    dialect: dbConf.dialect,
    pool: dbConf.pool,
    operatorsAliases: dbConf.operatorsAliases
});
sequelize.sync().then(() => console.log('db is connected.')).catch(() => console.log('db connection failed.'));  
module.exports = sequelize;