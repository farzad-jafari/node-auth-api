# api-auth
## build with:
* postgres, sequelize
* nodejs
* express
* passport, passport-jwt
* bcrypt

## dependency
* postgres
* node, npm, nodemon

## run
```
checkout project
cd project-path
$ npm install
$ npm start

```

## api-urls:

* post localhost:3000/users/signup

```
sample: 

POST /users/signup HTTP/1.1
Host: localhost:3000
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache
Postman-Token: 4b3a6fde-6f15-e744-258a-9120aa3a74ce

email=dddd8.jafari8990%40gmail.com&password=111&firstname=farzad&lastname=jafari

```

* post localhost:3000/users/login

```
POST /users/login HTTP/1.1
Host: localhost:3000
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache
Postman-Token: b699aaa7-76cf-bee4-49d3-b7c9e80a926a

email=dddd8.jafari8990%40gmail.com&password=111

```

* get localhost:3000/users/myprofile

```
GET /users/myprofile HTTP/1.1
Host: localhost:3000
Authorization: bearer eyJhbGciOiJIUzI1NiJ9.MTQ.xqKLCw7p1d8qefzd1_CJ1HBlB12sl5WKSanNcJn84dU
Cache-Control: no-cache
Postman-Token: 8a266d5e-3211-73a0-c9a2-a7c4ecff7f0d

```

### to get myprofile you need to set authorization in header such as below

```
Authorization: bearer <token>
```